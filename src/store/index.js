import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
	user: localStorage.user ? localStorage.user : 'admin',
	siteName: 'Advarus',
	token: localStorage.token ? localStorage.token : null,
	tokenExpires: 1531564690,
	apiDomain: process.env.NODE_ENV === 'development' ? 'http://advarus.pi/api/' : '/api/',
	//apiDomain: 'https://advarus.ru/api/'
};

export default new Vuex.Store({
  state
})
