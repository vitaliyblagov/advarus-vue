import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import Stats from '@/components/Stats'
import Analytics from '@/components/Analytics'
import Partners from '@/components/Partners'
import Sites from '@/components/Sites'
import Fraud from '@/components/Fraud'
import Login from '@/components/Login'
import Postbacks from '@/components/Postbacks'
import Home from '@/components/Home'

Vue.use(Router)

export default new Router({
	mode: 'history',
	routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        title: 'Home',
        isPublic: true
      }
    },
		{
			path: '/dashboard',
			name: 'Dashboard',
			component: Dashboard,
			meta: {
				title: 'Dashboard',
        isPublic: false
			}
		},
		{
			path: '/stats/:statType',
			name: 'Stats',
			component: Stats,
			props: true,
			meta: {
				title: 'Statistics',
        isPublic: false
			}
		},
		{
			path: '/analytics/:statType',
			name: 'Analytics',
			component: Analytics,
			props: true,
			meta: {
				title: 'Analytics',
        isPublic: false
			}
		},
		{
			path: '/partners',
			name: 'Partners',
			component: Partners,
			props: true,
			meta: {
				title: 'Partners',
        isPublic: false
			}
		},
		{
			path: '/partners/:id/postbacks',
			name: 'Postbacks',
			component: Postbacks,
			props: true,
			meta: {
				title: 'Postbacks',
        isPublic: false
			}
		},
		{
			path: '/sites',
			name: 'Sites',
			component: Sites,
			props: true,
			meta: {
				title: 'Sites',
        isPublic: false
			}
		},
		{
			path: '/login',
			name: 'Login',
			component: Login,
			props: true,
			meta: {
				title: 'Login',
        isPublic: true
			}
		},
    {
      path: '/fraud',
      name: 'Fraud',
      component: Fraud,
      props: true,
      meta: {
        title: 'Fraud',
        isPublic: false
      }
    }
	],
	beforeEach: function(to, from, next){
		document.title = to.meta.title;
		next();
	}
})
